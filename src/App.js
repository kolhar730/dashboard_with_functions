import React, { Component } from 'react';

import {
  TopAppBar,
  TopAppBarRow,
  TopAppBarSection,
  TopAppBarNavigationIcon,
  TopAppBarActionItem,
  TopAppBarTitle
} from 'rmwc/TopAppBar';

import {
  Drawer,
  DrawerContent,
  DrawerHeader
} from 'rmwc/Drawer';

import {
  List,
  SimpleListItem,
  ListItem,
  ListItemText,
} from 'rmwc/List';

import { Button } from 'rmwc/Button';
import { TabBar, Tab, TabIcon, TabIconText, TabBarScroller } from 'rmwc/Tabs';
import { ShapeContainer } from 'rmwc/Shape';
import { Card, CardPrimaryAction, CardMedia } from 'rmwc/Card';

import 'material-components-web/dist/material-components-web.min.css';
import './App.css';
import { Icon } from 'rmwc/Icon';

const drawerStyle = {
  paddingTop: '60px',
  transition: '0.5s ',
  position: 'fixed'
}


class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      tempOpen: false,
      activeTabIndex3: 1
    }
    this.subListOpen = false
    this.classOfIcon = 'notOpen'
    this.classOfSubDraw = 'subMenu'
    this.clickSubMenu = this.clickSubMenu.bind(this)
  }

  clickSubMenu () {
    let open = !this.subListOpen;
    this.subListOpen = open;
    console.log(open);
    if (open === true) {
      this.classOfIcon = 'turnIcon';
      this.classOfSubDraw = 'subMenu open';
    } else {
      this.staclassOfIcon = 'notOpen';
      this.classOfSubDraw = 'subMenu';
    }
  }
  
  render() {

    return (
      <div className="App">
      
      <div className="topAppBar">
      
      <TopAppBar short>
        <TopAppBarRow>
          <TopAppBarSection alignStart>
            <TopAppBarNavigationIcon use="menu" onClick={() => this.setState({tempOpen: !this.state.tempOpen})} />
              <TopAppBarTitle>College_Name Dashboard</TopAppBarTitle>
          </TopAppBarSection>
          <TopAppBarSection alignEnd>
            <TopAppBarActionItem aria-label="Log Out" alt="Log Out" title="Log Out">power_settings_new</TopAppBarActionItem>
          </TopAppBarSection>
        </TopAppBarRow>
      </TopAppBar>
      
      
      <Drawer className="drawer" style={drawerStyle} permanent>
        <DrawerContent>
          <ListItem>
            <ListItemText>Option 1</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Option 2</ListItemText>
          </ListItem>
          <ListItem className="withSubMenu" onClick={this.clickSubMenu} ripple style={{cursor:'pointer'}}>
            <ListItemText>Option 3</ListItemText>
            <Icon refs="icon_sub" className={this.classOfIcon} strategy="ligature" style={{marginLeft:'120px'}}>chevron_right</Icon>
          </ListItem>

          {/* Just a trial */}
          <div className={this.classOfSubDraw} refs="newSub">
            <ListItem>
              <ListItemText>Option</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemText>Option</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemText>Option</ListItemText>
            </ListItem>
            </div>

          <ListItem>
            <ListItemText>Option 3</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Option 3</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Option 3</ListItemText>
          </ListItem>
        </DrawerContent>
      </Drawer>

      <Drawer className="tempDrawer" temporary open={this.state.tempOpen} onClose={() => this.setState({tempOpen: false})}>
        <DrawerHeader>
          Twenty-Three ML
        </DrawerHeader>
        <DrawerContent>
          <ListItem>
            <ListItemText>Option1</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Option2</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Option3</ListItemText>
          </ListItem>
        </DrawerContent>
        </Drawer>
      
      </div>
      
      <div className="mainContent">
        
        <div className="cardGroup1">
            <ShapeContainer
              topLeftCorner="30"
              bottomRightCorner="30"
              outlineWidth="1"
              outlineColor="#e0e0e0"
              backgroundColor="#f2f2f2"
            >
              <Card outlined>
                <CardPrimaryAction>
                  <div style={{display: 'flex'}}>
                  <CardMedia square style={{
                    width: '110px',
                    backgroundColor: '#c4c4c4'
                    }}
                  />
                    <div style={{padding: '20px'}}>
                      Card
                    </div>
                  </div>
                </CardPrimaryAction>
              </Card>
            </ShapeContainer>

            <Card outlined>
                <CardPrimaryAction>
                  <div style={{display: 'flex'}}>
                  <CardMedia square style={{
                    width: '110px',
                    backgroundColor: '#c4c4c4'
                    }}
                  />
                    <div style={{padding: '20px'}}>
                      Card
                    </div>
                  </div>
                </CardPrimaryAction>
              </Card>

        </div>
        
        
        <div className="cardGroup2">
          <p>Main Map Section</p>
        </div>
                    

        <div className="cardGroup3">
          <Card outlined>
          <List twoLine>
            <SimpleListItem graphic="star_border" text="Cookies" secondaryText="Chocolate chip" meta="info" />
            <SimpleListItem graphic="favorite_border" text="Pizza" secondaryText="Pepperoni" meta="info" />
            <SimpleListItem graphic="mood" text="Icecream" secondaryText="Chocolate cookie dough" meta="info" />
          </List>
          </Card>
        </div>

      

      </div>
                    
      <div className="footer">
        <p>&copy; Twenty Three ML Studios - 2018</p>          
      </div>
      

      </div>
    );
  }
}

export default App;
