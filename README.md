# Dashboard with Functions

This is where all the action happens. 
So, to get involved, let's get these steps out of the way: 
	
- update your create-react-app or install it if you haven't got it. 
		```npm i -g create-react-app```
- clone this bad boy! (repo)
	
- run npm install after 

	```cd```ing into the source folder.
	
- install rmwc (a wrapper for material design components for react)
	```npm i rmwc --save``` or ```yarn add rmwc```
		
- run! using yarn start or npm start
	
So I will leave the master branch as it is. I will keep editing the source code and will putting all of the stuff on review from y'all and pushing onto master.
Only when I and you all are satisfied or the 💚 signal is given.

Let's fucking build this Dashboard! 
Also if there's any problem with this, let me know!

